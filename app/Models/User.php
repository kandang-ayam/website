<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class User extends Model
{
    protected $table = 'user';
	protected $primaryKey = 'id';
	static $rules = ['nama' => 'required', 'email' => 'required','foto'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:16384'];
	protected $perPage = 20;
	protected $fillable = ['nama','foto','email','job','password'];
}