<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=User::first()->paginate(20);
		return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
		return view('user.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        	//cek validasi
		request()->validate(User::$rules);
		//mulai transaksi
		\DB::beginTransaction();
		try
		{
      $tujuan_upload = 'foto';
      //simpan ke tabel kota
      \DB::table('user')->insert(['nama'=>$request->nama,
										'foto'=>$request->foto->move($tujuan_upload, $request->foto->getClientOriginalName()),
										'email'=>$request->email,
                    'job'=>$request->job,
                    'password'=>$request->password]);										
			//jika tidak ada kesalahan commit/simpan
			\DB::commit();
			return redirect()->route('user.index')->with('success', 'user created successfully.');
		} 
		catch (\Throwable $e) 
		{
			//jika terjadi kesalahan batalkan semua
			\DB::rollback();
			return redirect()->route('user.index')->with('success', 'Penyimpanan dibatalkan semua, ada kesalahan......');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user =User::find($id);
		return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =User::find($id);
		return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(User::$rules);
        $tujuan_upload = 'foto';
		//mulai transaksi
		\DB::beginTransaction();
		try
		{
			\DB::table('user')->where('id',$id)->update(['nama'=>$request->nama,
														'foto'=>$request->foto->move($tujuan_upload, $request->foto->getClientOriginalName()),
														'email'=>$request->email,
														'job'=>$request->job,'password'=>$request->password]);
			//$user->update($request->all());
			\DB::commit();
			return redirect()->route('user.index')->with('success', 'User updated successfully');
		}
		catch (\Throwable $e) 
		{
			//jika terjadi kesalahan batalkan semua
			\DB::rollback();
			return redirect()->route('user.index')->with('success', 'User Batal diubah, ada kesalahan');
		} 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //mulai transaksi
		\DB::beginTransaction();
		try
		{
			//hapus rekaman tabel kota
			$user =User::find($id)->delete();
			\DB::commit();
			return redirect()->route('user.index')->with('success', 'user deleted successfully');
		} 
		catch (\Throwable $e) 
		{
			//jika terjadi kesalahan batalkan semua
			\DB::rollback();
			return redirect()->route('user.index')->with('success', 'user ada kesalahan hapus batal... ');
		}
    }
}
