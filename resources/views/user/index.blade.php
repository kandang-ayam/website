@extends('app')
@section('title', 'User')

@section('content_header')
	<h2>Tabel User</h2>
@stop

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: spacebetween; align-items: center;">
						<span id="card_title">
							<h3> {{ __('User') }} </h3>
						</span>
						<div class="float-right">
							@include('user.search',['url'=>'user','link'=>'user'])
						</div>
					</div>
				</div>
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
				@endif
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead class="thead">
								<tr>
									<th>Id</th>
									<th>Foto</th>
									<th>Nama</th>
									<th>E-Mail</th>
									<th>Job</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($user as $usr)
								<tr>
								    <td>{{ $usr->id }}</td>
									<td><img src="{{ $usr -> foto }}" style="width: 50px; height: 50px; border-radius: 50%;" alt="foto"></td>
									<td>{{ $usr->nama }}</td>
									<td>{{ $usr->email }}</td>
									<td>{{ $usr->job }}</td>
									<td>
									<form action="{{ route('user.destroy',$usr->id) }}" method="POST">
										<a class="btn btn-sm btn-primary" href="{{ route('user.show',$usr->id) }}">
											<i class="fa fa-fw fa-eye"></i> Lihat</a>
										<a class="btn btn-sm btn-success" href="{{ route('user.edit',$usr->id) }}">
											<i class="fa fa-fw fa-edit"></i> Ubah</a>
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger btn-sm">
											<i class="fa fa-fw fa-trash"></i> Hapus</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			{!! $user ?? ''->links() !!}
		</div>
	</div>
 </div>
@endsection
