@extends('app') 
@section('title', 'User') 
@section('content_header') 
<h2> {{ $user->nama ?? 'Show User' }}</h2>
 @stop @section('content') 
 <section class="content container-fluid"> 
     <div class="row"> <div class="col-md-12"> 
         <div class="card"> 
             <div class="card-header"> 
                 <div class="float-left"> 
                     <span class="card-title">
                         <h3>Lihat User</h3>
                     </span> 
                 </div> 
                 <div class="float-right"> 
                     <a class="btn btn-primary" href="{{ route('user.index') }}"> Kembali</a> 
                </div> 
            </div> 
            <div class="card-body">
                <table width=80%>
                    <tr>
                <div class="form-group">
                    <td> 
                    <strong>Nama:</strong>
                    </td>
                    <td> {{ $user->nama }} </td>
                </div> 
                    </tr>
                    <tr>
                <div class="form-group">
                    <td> 
                    <strong>E-mail:</strong>
                    </td> 
                    <td>{{ $user->email }}</td> 
                </div>
                    </tr> 
                    <tr>
                <div class="form-group"> 
                    <td>
                    <strong>Job:</strong>
                    </td>
                    <td> {{ $user->job }}</td> 
                </div> 
                    </tr>
                    <tr>
                <div class="form-group">
                    <td> 
                    <strong>Foto: </strong>
                    <td><img src="{{ URL::to('/') }}/{{ $user -> foto }}" style="width: 100px; height: 100px;border-radius: 50%" alt="foto"> </td> 
                </div> 
                    </tr>
                </table>
            </div>
        </div> 
    </div> 
</div>
</section> 
@endsection