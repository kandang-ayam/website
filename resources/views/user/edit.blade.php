@extends('app')
@section('title', 'User')
@section('content_header')
 <h2>User</h2>
@stop
@section('content')
<section class="content container-fluid">
 <div class="row">
 <div class="col-md-12">
 @includeif('partials.errors')
 <div class="card card-default">
 <div class="card-header">
 <span class="card-title"><h3>Mengubah Data User </h3></span>
 </div>
 <div class="card-body">
 <form method="POST" action="{{ route('user.update',$user->id) }}" role="form" enctype="multipart/form-data">
 {{ method_field('PATCH') }}
 @csrf
 @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama</label>                    
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $user->nama }}" aria-describedby="nama" >                
                </div>
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="text" name="email" class="form-control" id="email" value="{{ $user->email }}" aria-describedby="email" >                
                </div>
				<div class="form-group">
                    <label for="job">Job</label>                    
                    <input type="text" name="job" class="form-control" id="job" value="{{ $user->job }}" aria-describedby="job" >                
                </div>
                <div class="form-group">
                    <label for="writer">Password</label>                    
                    <input type="password" name="password" class="form-control" id="password" value="{{ $user->password }}" aria-describedby="password" >                
                </div>
				<div class="form-group">
                    <label for="foto">Foto</label>                    
                    <input type="file" name="foto" class="form-control" id="foto" value="{{ $user->foto }}" aria-describedby="foto" >                
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
 </form>
 </form>
 </div>
 </div>
 </div>
 </div>
 </section>
@endsection