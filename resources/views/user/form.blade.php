<div class="box box-info padding-1">
 <div class="box-body">

 <div class="form-group">
 {{ Form::label('nama') }}
 {{ Form::text('nama', $user->nama, ['class' => 'form-control' . ($errors->has('nama') ? ' is-invalid' : ''), 'placeholder' => 'Nama']) }}
 {!! $errors->first('nama', '<div class="invalid-feedback">:message</p>') !!}
 </div></div>
 <div class="form-group">
 {{ Form::label('email') }}
 {{ Form::text('email', $user->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'E-mail']) }}
 {!! $errors->first('email', '<div class="invalid-feedback">:message</p>') !!}
 </div></div>
 <div class="form-group">
 {{ Form::label('job') }}
 {{ Form::text('job', $user->job, ['class' => 'form-control' . ($errors->has('job') ? ' is-invalid' : ''), 'placeholder' => 'Job']) }}
 {!! $errors->first('job', '<div class="invalid-feedback">:message</p>') !!}
 </div></div>
 <div class="form-group">
 {{ Form::label('password') }}
 {{ Form::password('password', $user->password, ['class' => 'form-control' . ($errors->has('password') ? ' is-invalid' : ''), 'placeholder' => 'Password']) }}
 {!! $errors->first('password', '<div class="invalid-feedback">:message</p>') !!}
 </div></div>
  <div class="form-group">
 {{ Form::label('foto') }}
 {{ Form::file('foto', $user->foto, ['class' => 'form-control' . ($errors->has('foto') ? ' is-invalid' : ''), 'placeholder' => 'Foto']) }}
 {!! $errors->first('foto', '<div class="invalid-feedback">:message</p>') !!}
 </div></div>
 <div class="box-footer mt20">
 <button type="submit" class="btn btn-primary">Simpan</button>
</div>
</div>